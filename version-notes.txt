VERSION V33 10/21/18
SINGLE FILE UPLOAD via imageupload.sh
mods to upload files in data/picbuf with individual calls per file rather than with one api call
 lightupdate.py
 imageupload.py
FREE SPACE CHECK
Run a check for free space on boot and 11:30pm every day
 freespace.py
 startup.py
 exolab.conf (this is where the setting for what the mimimum free space threshold is in MB)
INSTALLER notes
 installer.sh
 add freespace.py and exolab.conf to files "cp" 
 add new crontab editor to add crontab for freespace.py in to existing crontab (making sure not to duplicate it).  
  
VERSION 32 FINAL 9/18/18
on 10/11/18
version.txt is set to 15 so as to ensure no ExoLab will install from this when it's merged with the production repo
update.sh is configured to download from the production exolab_update repo rather than the dev version

