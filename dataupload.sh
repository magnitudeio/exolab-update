#!/bin/bash

# The Basic Auth credentials that go in every request.
#client_id="some.device.id"
#client_secret="example-client-secret"
basic_auth="$1"

#basic_auth="$client_id:$client_secret"
api_base="https://magni.magnitude.io/api/v1"

# Example 1
# ---------
# Uplad rows with no attached files

# A csv file to upload
file="data/buf.csv"

# The tag of the rows
tag="exo"

# Send the request
IP=$(curl --user "$basic_auth"        \
     -H "Content-Type: text/csv" \
     -X POST                     \
     --data-binary "@$file"      \
     "$api_base/streams/data/$tag/")

echo "$IP"



