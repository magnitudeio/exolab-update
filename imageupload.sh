#!/bin/bash

# The Basic Auth credentials that go in every request.
#basic_auth="$client_id:$client_secret"
. /home/pi/ExoLab/const.sh
basic_auth=$2
#V33 now assuming this will only ever upload a single file.  Pass the file path in to this as argument 3
filename=$3

# We use this to get S3 upload URIs from the response body.
get_upload_uri() {
 read -d '' code <<EOF
import json, sys

body = json.loads(sys.stdin.read())
uris = filter(None, body["upload_uri"]["$1"])

for uri in uris:
  print uri

EOF

  python -c "$code"
}

# Example 2
# ---------
# Upload rows and upload a file for each non empty cell
# of a certain column, with the cell value as the name to
# assign to the file in S3.

# Column to upload
upload_file_column="filename"

# Sample JSON body
read -d '' body <<EOF
$1
EOF
echo $body

# The tag of the rows
tag="exo/pics"

# Command to get the image filenames
#V33 no longer used - as we will only ever send one file at a time
get_filenames() {
  command ls data/picbuf/*
}

# Send the requests first to the API then to S3.
# The request sent to S3 is uses the http method PUT, not POST.
curl -s --user $basic_auth \
     -H "Content-Type: application/json" \
     -X POST                             \
     -d "$body"                          \
     "$API_BASE/streams/data/$tag/"      \
| tee /dev/tty                           \
| cat - <(echo)                          \
| get_upload_uri $upload_file_column     \
| paste -d '\n' - <(echo "$filename")       \
| while read -r upload_uri && read -r filepath
do
  if [ -z upload_uri ] || [ -z filepath ]
  then
    break
  fi

  echo -e "Uploading file '$filepath' to URI '$upload_uri' ."
  curl --upload-file $filepath $upload_uri
done



