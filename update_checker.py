import sys
sys.dont_write_bytecode=True
import subprocess
import urllib2
import LCD
import json
import os
import const

def available():
    version_file=open(const.const['VERSION_FILENAME'],'r')
    try: version=int(version_file.read())
    except: version = 0
    version_file.close()
    print const.const['SOFTWARE_URL']

    try:
        req = urllib2.Request(const.const['SOFTWARE_URL'], headers={ 'User-Agent': 'Mozilla/5.0' })
        remote_version = int(urllib2.urlopen(req).read())
    except:
        remote_version = 0
    print remote_version
    return (remote_version > version)

def update():
    LCD.start()
    LCD.lcd_string("UPDATING...",LCD.LCD_LINE_1)
    LCD.lcd_string("",LCD.LCD_LINE_2)
    pipe=subprocess.Popen("sudo bash /home/pi/ExoLab/update.sh", shell=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    out,err=pipe.communicate()
    result=out.decode()

if __name__ == '__main__':
    if available():
        update()
