#! /bin/bash

# first check if this device has been configured to use HTPDate by user
if [ -f "/home/pi/ExoLab/use-htpd-time-sync" ]
then
  dateFromServer=$(curl --silent https://magni.magnitude.io/public/server-time | grep -o '"serverTime":"[^"]*' | cut -d\" -f4)
  echo "dateFromServer: $dateFromServer"
  echo "...disabling ntp..."
  sudo timedatectl set-ntp false
  echo "...Setting Time..." 
  sudo timedatectl set-time "$dateFromServer"
fi
