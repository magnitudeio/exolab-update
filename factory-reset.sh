#!/bin/bash

echo "Time: $(date). Restoring ExoLab data & files." | sudo tee -a /home/pi/ExoLab/update.log

# clear data buffer file
cp /home/pi/ExoLab/data/bufb.csv /home/pi/ExoLab/data/buf.csv

# clear images buffer folder
sudo rm -rvf /home/pi/ExoLab/data/picbuf/*

if [ -d "/home/pi/backup" ]
then
    echo "Time: $(date). /home/pi/backup exists and will be used to restore ExoLab files/folders" | sudo tee -a /home/pi/ExoLab/update.log
    cd /home/pi/backup/
    
    cp libexoledmatrix.so /home/pi/ExoLab
    cp lightupdate.py /home/pi/ExoLab/

    #get current version
    CVN=$(cat /home/pi/ExoLab/version.txt)

    #Update-specific commands

    if [ "$CVN" -lt 15 ]; then
	    sudo rm /home/pi/ExoLab/update_checker.pyc
	    fullcred=$(cat /home/pi/ExoLab/cred)
	    cred=${fullcred:0:7}
	    echo $cred
	    sudo sed -i "4s/.*/ssid=ExoLab:$cred/" /etc/hostapd/hostapd.conf
    fi

    #Regular cp of system scripts

    # Copy new dhcpcd configuration files over to their respective locations
    # NOTE: Each configuration file is used at different times during the function of the ExoLab.
    # dhcpcd-setup: Used when the ExoLab is put into Network Setup mode in order to broadcast as an Access Point;	this config sets
    # the wlan0 interface.
    # dhcpcd-clean: Used when the ExoLab is functioning in Experiment Mode.
    #cp dhcpcd-setup.conf /home/pi/ExoLab/network_setup/dhcpcd.conf
    #cp dhcpcd-clean.conf /home/pi/ExoLab/network_clean/dhcpcd.conf
    # reset copies inside ExoLab folder as well
    cp dhcpcd-setup.conf /home/pi/ExoLab/
    cp dhcpcd-clean.conf /home/pi/ExoLab/
    # Copy dhcpcd hook which allows the device to look for the time sync from a local
    # DHCP server.
    #sudo cp 60-timesyncd.conf /lib/dhcpcd/dhcpcd-hooks/
    # reset copy inside ExoLab folder
    cp 60-timesyncd.conf /home/pi/ExoLab/

    # This update includes updates to flask/run.py and flask/app/views.py
    cp -r flask /home/pi/ExoLab/
    cp APclean.sh /home/pi/ExoLab/

    if [[ `cat /proc/cpuinfo | grep "Raspberry Pi 3 Model A"` ]]; then
            cp APsetupnew.sh /home/pi/ExoLab/APsetup.sh
    else
            cp APsetup.sh /home/pi/ExoLab/
    fi

    cp color.json /home/pi/ExoLab/
    cp const.py /home/pi/ExoLab/
    cp const.sh /home/pi/ExoLab/

    cp prod.py /home/pi/ExoLab/
    cp startup.py /home/pi/ExoLab/
    cp update_checker.py /home/pi/ExoLab/
    cp freespace.py /home/pi/ExoLab/
    cp exolab.conf /home/pi/ExoLab/

    chmod u+x update.sh
    cp update.sh /home/pi/ExoLab/

    cp imageupload.sh /home/pi/ExoLab/
    cp dataupload.sh /home/pi/ExoLab/

    # Hotfix for imageupload.sh with Windows line endings
    # Really should not need this anymore.. leaving here for now.  
    cp /home/pi/ExoLab/imageupload.sh /home/pi/ExoLab/imageupload.sh.backup
    sudo tr -d '\r' < /home/pi/ExoLab/imageupload.sh.backup > /home/pi/ExoLab/imageupload.sh

    # LK DEV/PROD Switcher Script
    cp switchdev.sh /home/pi/ExoLab/

    cp factory-reset.sh /home/pi/ExoLab/

    # just make sure all .sh scripts are executable 
    sudo chmod +x /home/pi/ExoLab/*.sh

    # Hotfix for crontab update_checker problem
    echo "Time: $(date). Crontab repair" | sudo tee -a /home/pi/ExoLab/update.log
    ( sudo crontab -l -u pi | grep -v -E "update_checker" ; echo "0 0 * * * sudo python /home/pi/ExoLab/update_checker.py" ) | crontab - -u pi

    # V33 freespace check.  Edit crontab
    # IMPORTANT: MUST BE AFTER the crontab edit above for the update checker fix
    echo "Time: $(date). Crontab edit for FREESPACE check" | sudo tee -a /home/pi/ExoLab/update.log
    (sudo crontab -l -u pi | grep -v '^#'; echo "30 23 * * * sudo python /home/pi/ExoLab/freespace.py") | sort | uniq | crontab - -u pi

    #Copy of version
    cp version.txt /home/pi/ExoLab/

else
    echo "Time: $(date). /home/pi/backup directory not present. Skipping ExoLab files reset." | sudo tee -a /home/pi/ExoLab/update.log
fi

echo "Time: $(date). Factory Restore complete: Reboot in 7 seconds" | sudo tee -a /home/pi/ExoLab/update.log

sudo ./APclean.sh
