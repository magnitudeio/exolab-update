# V33 10/21/18 LK
# Add freespace check on boot
import sys
sys.dont_write_bytecode=True #Prevent a bunch of compiled python files.
import pigpio
import subprocess
import time
import LCD
import update_checker
import const

#Start GPIO daemon and display to show startup messages
pi=pigpio.pi()
pi.set_mode(16,pigpio.INPUT)
LCD.start()

#Get version and Serial number from respective files
credfile=open(const.const['CRED_FILENAME'])
cred=credfile.read()[0:7]
verfile=open(const.const['VERSION_FILENAME'])
ver=verfile.read()
if ver == "":
    ver = '0'
ver=ver.strip()

# V33 run freespace check on startup.    
subprocess.Popen('python /home/pi/ExoLab/freespace.py', shell=True)

#Display VN and SN
LCD.lcd_string("SN: "+cred,LCD.LCD_LINE_1)
LCD.lcd_string("VN: "+ver,LCD.LCD_LINE_2)
time.sleep(3)

#Prompt for setup mode.
LCD.lcd_string("  HOLD BUTTON",LCD.LCD_LINE_1)
LCD.lcd_string("FOR NETWORK MGMT",LCD.LCD_LINE_2)
time.sleep(5)

if(pi.read(16)):
    pi.stop()
    LCD.lcd_string("ENTERING NETWORK",LCD.LCD_LINE_1)
    LCD.lcd_string("  SETUP MODE...",LCD.LCD_LINE_2)
    time.sleep(5)
	#Start shell script that sets up the access point.
    subprocess.Popen('/home/pi/ExoLab/APsetup.sh')
else:	#Check for update
    time.sleep(15)
    try: update_available = update_checker.available()
    except: update_available = False

	#Checks if files are still configured for AP (Can occur if device is not rebooted cleanly with APclean.sh)
    if 'wlan0' in open("/etc/dhcpcd.conf").read():
        LCD.lcd_string("CLEANING SETUP",LCD.LCD_LINE_1)
        LCD.lcd_string(" ",LCD.LCD_LINE_2)
        time.sleep(2)
        subprocess.Popen('/home/pi/ExoLab/APclean.sh') #Clean up AP files for Experiment Mode operation
    elif update_available:	#Update now if possible. There is a delay between checking for update and attempting update, which is somewhat problematic.
        update_checker.update()
    else:	#Go into Experiment mode, which is for normal operation
        subprocess.Popen('/home/pi/ExoLab/set-time-from-http.sh')
    	LCD.lcd_string("EXPERIMENT MODE",LCD.LCD_LINE_1)
    	LCD.lcd_string(" ",LCD.LCD_LINE_2)
    	subprocess.Popen('python /home/pi/ExoLab/prod.py', shell=True) #Start data handling script.
    	time.sleep(7)
    	subprocess.Popen('python /home/pi/ExoLab/lightupdate.py', shell=True) #Start light and camera script.
