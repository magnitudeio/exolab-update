#! /bin/bash

sudo pkill -f prod
echo "Copying config..."
sudo cp /home/pi/ExoLab/network_setup/* /etc/
echo "Reloading config..."
sudo systemctl daemon-reload
echo "Restarting dhcpcd..."
sudo service dhcpcd restart
echo "Restarting hostapd..."
sudo service hostapd restart
echo "Restarting dnsmasq..."
sudo service dnsmasq restart
sudo python /home/pi/ExoLab/flask/run.py
