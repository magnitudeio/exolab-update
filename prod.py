#!/usr/bin/python
# -*- coding: utf-8 -*-
#Version 26

import pigpio
import time
from datetime import datetime
import math
import os
import re
import subprocess
import sys
import serial
from shutil import copyfile
import LCD

print("Starting")

#elements to build json package
os.chdir("/home/pi/ExoLab")
json_open='\{\\"data\\":['
ts_open='\{\\"timestamp\\":\\"'
ts_close='\\",\\"filename\\":\\"image.jpg\\"\}'
json_close='],\\"upload_file\\":[\\"filename\\"]\}'

thu_address=0x40
lum_address=None #To be determined

sample_period=10 #seconds between data samples
blink_period=4
samples_per_point=90 #number of data points to be average together

#Where the data goes
datafolder = "data/"
dataformat='.csv'
bufname= datafolder+'buf'+dataformat
bufbname= datafolder+'bufb'+dataformat
header="Time,Temp,Hum,CO2,lux\n"

credfile=open("cred")
cred=' '+credfile.read()[:24] # hotfix v34 was [:-1]

def start_lum():
    global lum_address
    available_ports = ""
    try:available_ports=subprocess.check_output('i2cdetect -y 1', shell=True)[:-1]
    except:available_ports = ""
    
    if "44" in available_ports:
        lum_address = 0x44
        lum_handle = pi.i2c_open(1, lum_address)
        start_lum_old(lum_handle)
    elif "10" in available_ports:
        lum_address = 0x10
        lum_handle = pi.i2c_open(1, lum_address)
        start_lum_new(lum_handle)
    else:
        print("ERROR no lux detected")

    lum_handle = pi.i2c_open(1, lum_address)
    return lum_handle
    


def start_lum_old(lum_handle): #configure light sensor to start and take readings
	DEVICE =0x00
	CONFIG_1=0x01
	CONFIG_2=0x02
	CONFIG_3=0x03

	lumreset=pi.i2c_write_byte_data(lum_handle,DEVICE,0x46)
	lumconfig1=pi.i2c_write_byte_data(lum_handle,CONFIG_1, 0x0D)
	lumconfig2=pi.i2c_write_byte_data(lum_handle,CONFIG_2, 0x3F)
	lumconfig3=pi.i2c_write_byte_data(lum_handle,CONFIG_3, 0x00)

def start_lum_new(lum_handle):
    pi.i2c_write_word_data(lum_handle, 0x00, 0x0000)

def get_temp(thu_handle):
	# HDC1000 address, 0x40(64)
	# Send temp measurement command, 0x00(00)
	pi.i2c_write_byte(thu_handle, 0x00)

	time.sleep(0.5)

	# HDC1000 address, 0x40(64)
	# Read data back, 2 bytes
	# temp MSB, temp LSB
	data0 = pi.i2c_read_byte(thu_handle)
	data1 = pi.i2c_read_byte(thu_handle)

	# Convert the data
	temp = (data0 * 256) + data1
	T_c = (temp / 65536.0) * 165.0 - 40
	T_c =(T_c+22.1)/2

	return T_c

def get_hum(thu_handle):
	# HDC1000 address, 0x40(64)
	# Send humidity measurement command, 0x01(01)
	pi.i2c_write_byte(thu_handle, 0x01)

	time.sleep(0.5)

	# HDC1000 address, 0x40(64)
	# Read data back, 2 bytes
	# humidity MSB, humidity LSB
	data0 = pi.i2c_read_byte(thu_handle)
	data1 = pi.i2c_read_byte(thu_handle)

	# Convert the data
	humidity = (data0 * 256) + data1
	H_p = (humidity / 65536.0) * 100.0
	return H_p

def get_light(lum_handle):
    if lum_address == 0x44:
        return get_light_old(lum_handle)
    elif lum_address == 0x10:
        return get_light_new(lum_handle)
    else:
        return None

def get_light_old(lum_handle):
	lightfactor=850
	RGB_reg=[0x0B,0x09,0x0D];
	broadspec=0
	colors=[0,0,0]

	for RGB_idx, color in enumerate(RGB_reg):
		pi.i2c_write_byte(lum_handle,color) #For each of the RGB registers, read the data.
		(count, color_data_array)= pi.i2c_read_device(lum_handle,2) 
		color_data_int=color_data_array[1]*256+color_data_array[0] #Convert to integer
		colors[RGB_idx]=color_data_int
	broadspec=sum(colors)/lightfactor	#sum the thre colors then adjust to umol m2 s-1
	return broadspec

def get_light_new(lum_handle):
    lightfactor=248
    regs = [0x05, 0x06, 0x07]
    
    vals = (pi.i2c_read_word_data(lum_handle, i) for i in regs)

    return sum(vals)/lightfactor

def get_co2(ser):
	measure='\xff\x01\x86\x00\x00\x00\x00\x00\x79' #Measure CO2 command
	ser.write(measure)
	co2raw=ser.read(9)
 	if len(co2raw) < 9:
 		return -1
	co2int=[ord(u) for u in co2raw] #convert to integer
	if co2int[0]!=255 and co2int[-1]==255:
		co2int= [co2int[-1]]+co2int[:-1]
	print co2int
	co2=255*co2int[2]+co2int[3]	#positions 2&3 are two-byte value for CO2
	return co2

def LCD_screen(line1,line2):
	LCD.start()
	LCD.lcd_string(line1,LCD.LCD_LINE_1)
	LCD.lcd_string(line2,LCD.LCD_LINE_2)

def data_to_file(filename, data):
	f= open(filename, 'a')
	f.write(data)
	f.close()

if __name__ == '__main__':
	while True:
		#start all the sensors
		pi=pigpio.pi()
		ser = serial.Serial('/dev/serial0', 9600,timeout=3.0)	
		thu_handle=pi.i2c_open(1, thu_address)
		lum_handle=start_lum()
		print("all started")
		try:
			while True:
				for samplen in range(0,samples_per_point):
					broadspec=get_light(lum_handle)
					H_p=get_hum(thu_handle)
					T_c=get_temp(thu_handle)
					co2=get_co2(ser)

					#Collect into array
					sample_data = [T_c,H_p,co2,broadspec]
					if samplen == 0:
						dataarray = [sample_data]
					else: dataarray.append(sample_data)
					
					#Display to stdout
					print("--------------------------------------------------")
					print("Temperature(C): {0:.2f}".format(T_c)+      ";   Humidity(%):      {0:.2f}".format(H_p))
					print("CO2(ppm);       {0:.2f}".format(co2) + ";   Brightness(lux):  {}".format(broadspec))
					ln1="Temp.: {0:.2f}\xdfC".format(T_c)
					ln2="Hum. : {0:.2f}%".format(H_p)
					ln3="CO2  : {} PPM".format(co2)
					ln4="Light: {} umol".format(broadspec)

					ssid_ln="WiFi: "
					try:ssid_ln=ssid_ln+subprocess.check_output("iwgetid -r", shell=True)[:-1]
					except:ssid_ln=ssid_ln+"N/A"

					try:
						IP_info=subprocess.check_output('hostname -I', shell=True)[:-1]
						IP_ln=re.search('\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}', IP_info).group(0)
					except:IP_ln="No IP"

					#Show on screen
					LCD_screen(ln1,ln2)
					time.sleep(blink_period)
					
					LCD_screen(ln3,ln4)
					time.sleep(blink_period)

					LCD_screen(ssid_ln,IP_ln)
					time.sleep(sample_period-2*blink_period)
					
				[T_cf,H_pf,co2f,broadspecf]= [sum([sample[k] for sample in dataarray])/float(len(dataarray)) for k in range(0,4)]
				 
				#Write date and data to file
				daten= datetime.now().date().isoformat()
				iso=datetime.utcnow().isoformat()+"000Z"
				filename=datafolder + daten + dataformat
				written=iso + ',' +str(T_cf)+ ',' +str(H_pf)+ ',' +str(co2f)+ ',' +str(broadspecf)+ '\n'

				data_to_file(filename,written)
				data_to_file(bufname,written)

				pipe=subprocess.Popen('sh dataupload.sh'+cred, shell=True, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
				out,err=pipe.communicate()
				result=out.decode()
				#if successful, rewrite the buffer file with a blank buffer file
				if "protocol41" in result:
					copyfile(bufbname,bufname)
				
		 
		except KeyboardInterrupt:
			print("Bye!")
			pi.i2c_close(lum_handle)
			pi.i2c_close(thu_handle)
			ser.close()
			pi.stop()
			sys.exit()
