#! /bin/bash

sudo pkill -f prod.py
sudo pkill -f lightupdate.py

. /home/pi/ExoLab/const.sh

cd /home/pi/ExoLab/
sudo rm -r update

curl -o latest.zip $LATEST_UPDATE_URL
unzip latest.zip -d "latest"
rm -f latest.zip
sudo mv latest update

cd /home/pi/ExoLab/update

chmod u+x installer.sh
sudo ./installer.sh
