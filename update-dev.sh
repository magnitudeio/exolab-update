#! /bin/bash

sudo pkill -f prod.py
sudo pkill -f lightupdate.py

. /home/pi/ExoLab/const.sh

cd /home/pi/ExoLab/
sudo rm -r update

git clone -b $DEV_BRANCH --single-branch $UPDATE_REPO
sudo mv exolab-update update

cd /home/pi/ExoLab/update

chmod u+x installer.sh
sudo ./installer.sh
