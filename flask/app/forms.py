from flask_wtf import Form
from wtforms import StringField, BooleanField, PasswordField, SelectField, SubmitField
from wtforms.validators import DataRequired, Length

class LoginForm(Form):
    myMAC = open('/sys/class/net/wlan0/address').read()
    ssid = StringField('ssid')
    psk = PasswordField('psk',id='psk')
    username = StringField('username', id='un')
    password = PasswordField('password',id='pw')
    security=SelectField(u'Security Protocol',choices=[('WPA-PSK',"WPA-PSK"),('NONE',"None"),("WPA-PAEP","WPA Enterprise/PEAP")])
    restart = SubmitField('Submit And Restart')
    clear = SubmitField('CLEAR NETWORK MEMORY')
    backup = SubmitField('RESET FACTORY FILES')
    setTime = SubmitField('SET DEVICE TIME FROM MAGNITUDE')
    setTimeNTP = SubmitField('SET DEVICE TIME USING NTP')
    reboot = SubmitField('Reboot ExoLab')
