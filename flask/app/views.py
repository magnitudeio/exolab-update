from flask import render_template, flash, redirect
from app import app
from .forms import LoginForm
import subprocess
import time
import LCD
import shutil
import time
import os

entry_WPA="""
network={{
\tssid=\"{}\"
\tpriority=2
\tpsk=\"{}\"
\tkey_mgmt=WPA-PSK
}}
"""

entry_NONE="""
network={{
\tssid=\"{}\"
\tpriority=2
\tkey_mgmt=NONE
}}
"""

entry_PAEP="""
network={{
\tssid=\"{}\"
\tpriority=2
\tkey_mgmt=WPA-EAP
\tproto=RSN
\tpairwise=CCMP
\tauth_alg=OPEN
\teap=PEAP
\tidentity=\"{}\"
\tpassword=\"{}\"
\tphase1="peaplabel=0"
\tphase2="auth=MSCHAPV2"
}}
"""

# index view function suppressed for brevity
@app.route('/', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        print form.restart
        print form.clear
        if form.restart.data:
            accepted = True
            if (form.security.data == 'WPA-PSK' and len(form.psk.data)>=8 ):
                new_entry=entry_WPA.format(form.ssid.data,form.psk.data,form.security.data)
            elif (form.security.data == 'WPA-PSK' and len(form.psk.data) < 8 ):
                flash('Passkey is not long enough')
                accepted = False
            elif (form.security.data=='NONE'):
                new_entry=entry_NONE.format(form.ssid.data,form.security.data)
            elif (form.security.data=='WPA-PAEP'):
                new_entry=entry_PAEP.format(form.ssid.data,form.username.data,form.password.data)

            if(accepted):
                flash('Credentials accepted. Restarting...')
                wpa=open("/etc/wpa_supplicant/wpa_supplicant.conf",'a')
                wpa.write(new_entry)
                wpa.close()
                LCD.start()
                LCD.lcd_string("RESTARTING...",LCD.LCD_LINE_1)
                LCD.lcd_string("PLEASE WAIT...",LCD.LCD_LINE_2)
                subprocess.Popen('/home/pi/ExoLab/APclean.sh')
            
        elif form.clear.data:
            shutil.copyfile("/home/pi/ExoLab/wpa_backup.conf","/etc/wpa_supplicant/wpa_supplicant.conf")
            LCD.start()
            LCD.lcd_string("CLEARING NTWKS ",LCD.LCD_LINE_1)
            LCD.lcd_string("AND RESTARTING ",LCD.LCD_LINE_2)
            subprocess.Popen('/home/pi/ExoLab/APclean.sh')

        elif form.backup.data:
            LCD.start()
            LCD.lcd_string("RESTORING",LCD.LCD_LINE_1)
            LCD.lcd_string("FACTORY DATA",LCD.LCD_LINE_2)
            time.sleep(2) # sleep for 2 seconds to make it easier to read display messages
            subprocess.Popen('/home/pi/ExoLab/factory-reset.sh')
            LCD.lcd_string("RESTARTING...",LCD.LCD_LINE_1)
            LCD.lcd_string("PLEASE WAIT...",LCD.LCD_LINE_2)

        elif form.setTime.data:
            LCD.start()
            LCD.lcd_string("SYNCING TIME", LCD.LCD_LINE_1)
            LCD.lcd_string("FROM MAGNITUDE SERVER", LCD.LCD_LINE_2)
            open('/home/pi/ExoLab/use-htpd-time-sync', 'a').close()
            time.sleep(4)
            flash('Magnitude.io servers will be used for time synchronization. You may now continue with your Network Setup or reboot the ExoLab.')

        elif form.setTimeNTP.data:
            LCD.start()
            LCD.lcd_string("SYNCING TIME", LCD.LCD_LINE_1)
            LCD.lcd_string("USING NTP", LCD.LCD_LINE_2)
            if os.path.exists('/home/pi/ExoLab/use-htpd-time-sync'):
                os.remove('/home/pi/ExoLab/use-htpd-time-sync')
            subprocess.Popen('sudo timedatectl set-ntp true', shell=True)
            time.sleep(4)
            flash('NTP server pools will be used for time synchronization. You may now continue with your Network Setup or reboot the ExoLab.')

        elif form.reboot.data:
            LCD.start()
            LCD.lcd_string("RESTARTING...",LCD.LCD_LINE_1)
            LCD.lcd_string("PLEASE WAIT...",LCD.LCD_LINE_2)
            subprocess.Popen('/home/pi/ExoLab/APclean.sh')

        
        return redirect('/')
    return render_template('login.html', 
                           title='WPA-2 Credentials',
                           form=form)
